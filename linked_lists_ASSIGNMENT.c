#include <stdio.h>
#include <stdlib.h>

//NALUWUJJA ASHLEY HOPE
//2300714174
//23/U/714174/PS

struct Node
{
    int number;
    struct Node *next;
};

struct Node *createNode(int num){
struct Node *newNode = malloc(sizeof(struct Node));
 if (newNode == NULL) {
        printf("Error: Unable to allocate memory for a new node\n");
        exit(1);
    }
newNode->number = num;
newNode->next = NULL;
return newNode;
}


void printList(struct Node *head){
    printf("[");
struct Node *current = head;
printf("%d",current->number);
current = current->next;
if(current != NULL){
            printf(", ");
        }
    printf("]\n");
}


void append(struct Node **head, int num)
{
    struct Node *newNode = createNode(num);
    if (*head == NULL)
    {
        *head = newNode;
        return;
    }
    struct Node *current = *head;
    while (current->next != NULL)
    {
        current = current->next;
    }
    current->next = newNode;
}

void prepend(struct Node **head, int num){
struct Node *newNode = createNode(num);
newNode->next = *head;
newNode = *head;
}

void deleteByKey(struct Node **head, int key){
    struct Node *current = *head;
    struct Node *prev = NULL;
    if(current != NULL && current->number == key){
    *head = current->next;
    free(current);
    return;}
while(current != NULL && current->number != key){
    prev = current;
    current = current->next;}
if (current == NULL){
    printf("key not found in the list");
    return;}
prev->next = current->next;
free(current);
}

void deleteByValue(struct Node **head, int value){
struct Node *current = *head;
struct Node *prev = NULL;
if(current != NULL && current->number == value){
    *head = current-> next;
    free(current);
    return;
}
while(current != NULL && current->number != value){
    prev = current;
    current = current->next;
    }

if(current == NULL){
    printf("value not found");
    return;
}
prev->next = current->next;
free(current);

}

void insertAfterKey(struct Node **head, int key, int value)
{
    struct Node *current = *head;
    if(current != NULL && current->number != key){
        struct Node *newNode = createNode(value);
        current->next = newNode;
        return;
    }
    while (current != NULL && current->number != key)
    {
        current = current->next;
    }
    if (current == NULL)
    {
        return;
    }
    struct Node *newNode = createNode(value);
    newNode->next = current->next;
    current->next = newNode;

}
void insertAfterValue(struct Node **head, int searchValue, int newValue)
{
    struct Node *current = *head;
     if(current != NULL && current->number != newValue){
        struct Node *newNode = createNode(newValue);
        current->next = newNode;
        return;
    }
    while (current != NULL && current->number != newValue)
    {
        current = current->next;
    }

    if (current == NULL)
    {
        return;
    }

    struct Node *newNode = createNode(newValue);
    newNode->next = current->next;
    current->next = newNode;
}

int main()
{
    struct Node *head = NULL;
    int choice, data;

    while (1)
    {
        printf("Linked Lists\n");
        printf("1. Print List\n");
        printf("2. Append\n");
        printf("3. prepend\n");
        printf("4. Delete\n");
        printf("5. Exit\n");
        printf("Enter your choice: ");
        scanf("%d", &choice);


        switch(choice)
        {case 1:
            printf("Linked List: ");
            printList(head);
            break;
        case 2:
            printf("Enter data to append: ");
            scanf("%d", &data);
            append(&head, data);
            break;
        case 3:
            printf("Enter data to prepend: ");
            scanf("%d", &data);
            prepend(&head, data);
            break;
        case 4:
            printf("Enter data to delete: ");
            scanf("%d", &data);
            deleteByValue(&head, data);
            break;
        case 5:
            printf("Exiting program.\n");
            exit(0);
            default:
            printf("Invalid choice. Please try again.\n");
            break;
        }
    }

    return 0;
}
